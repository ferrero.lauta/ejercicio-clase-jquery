function cambiarColorDiv() {
    let colorSeleccionado = $('#combo-color').val();
    if (colorSeleccionado) {
        $('#caja-color').css('background-color', colorSeleccionado);
    } else {
        alert('Debe seleccionar un color');
    }
}

// Acá espera que el documento esté cargado en el dom (las etiquetas HTML) para recién poder ahí manipularlo.
// Al estar cargado (ready), ejecutará la funcion que declaramos luego de "function()"
$(document).ready(function () {
    // Acá tomamos la referencia del elemento HTML con ID combo-color (la lista desplegable/select), y asigno esa referencia a la variable combo
    let combo = $('#combo-color');
    // Acá le digo que cuando el valor del "combo" cambie (change()) ejecute la función
    combo.change(function () {
        // El "this" es una referencia local del elemento que disparó la función, en este caso, es el equivalente a escribir nuevamente "combo"
        if ($(this).val()) { // Acá toma el valor seleccionado en el combo (con la función val)
            $('#caja-color').css('background-color', $(this).val()); // A través de la función CSS de la caja, le aplicamos el valor seleccionado en la lista desplegable
        } else {
            alert('Debe seleccionar un color');
        }
    });

    let contenedor = $('#caja-color');
    contenedor.dblclick(function () {
        $(this).css('background-color', 'white');
        $('#combo-color').val('');
    });
});
