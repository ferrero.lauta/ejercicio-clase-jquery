function agregarPersona() {
    let hayErrores = validarCampos();
    if (hayErrores === false) {
        let nombre = document.getElementById('nombre-usuario').value;
        let apellido = document.getElementById('apellido-usuario').value;
        let sexo = document.getElementById('combo-sexo').value;
        let tipoDocumento = document.getElementById('tipo-documento').value;
        let dni = document.getElementById('dni-usuario').value;
        let telefono = document.getElementById('telefono-usuario').value;

        let tabla = document.getElementById("tabla");

        // La tabla se inicializa escondida y si esta escondida, ejecutamos la funcion mostrar tabla.
        if (tabla.style.display === '') {
            mostrarTabla(tabla);
        }

        let fila = tabla.insertRow(tabla.length);
        fila.insertCell().innerHTML = nombre;
        fila.insertCell().innerHTML = apellido;
        fila.insertCell().innerHTML = sexo;
        fila.insertCell().innerHTML = tipoDocumento;
        fila.insertCell().innerHTML = dni;
        fila.insertCell().innerHTML = telefono;

        // document.getElementById('nombre-usuario').value = '';
        // document.getElementById('apellido-usuario').value = '';
        // document.getElementById('combo-sexo').value = '';
        // document.getElementById('tipo-documento').value = '';
        // document.getElementById('dni-usuario').value = '';
        // document.getElementById('telefono-usuario').value = '';

        $('input').val('');
        $('select').val('');

        $('span').hide();
    }
}

function mostrarTabla(tabla) {
    tabla.style.display = 'table';
}

function validarCampos() {
    let hayErrores = false;
    if ($('#nombre-usuario').val() === '') {
        $('#nombre-validacion').show();
        hayErrores = true;
    } else {
        $('#nombre-validacion').hide();
    }

    if ($('#apellido-usuario').val() === '') {
        $('#apellido-validacion').show();
        hayErrores = true;
    } else {
        $('#apellido-validacion').hide();
    }

    if ($('#combo-sexo').val() === '') {
        $('#sexo-validacion').show();
        hayErrores = true;
    } else {
        $('#sexo-validacion').hide();
    }

    if ($('#tipo-documento').val() === '') {
        $('#tipo-documento-validacion').show();
        hayErrores = true;
    } else {
        $('#tipo-documento-validacion').hide();
    }

    if ($('#dni-usuario').val() === '') {
        $('#dni-validacion').show();
        hayErrores = true;
    } else {
        $('#dni-validacion').hide();
    }

    if ($('#telefono-usuario').val() === '') {
        $('#telefono-validacion').show();
        hayErrores = true;
    } else {
        $('#telefono-validacion').hide();
    }

    return hayErrores;
}
