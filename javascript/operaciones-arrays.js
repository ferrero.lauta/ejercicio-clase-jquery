// Descomentar lo que quieran probar.
// Filter: filtra elementos de un array (lista)

// let listaElectrodomesticos = ['heladera', 'microondas', 'horno', 'pava', 'extractor', 'batidora', 'licuadora'];
//
// let listaAutos = [
//     {
//         marca: 'Toyota',
//         modelo: 'Etios',
//         anio: '2018'
//     },
//     {
//         marca: 'Toyota',
//         modelo: 'Yaris',
//         anio: '2021'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Focus',
//         anio: '2012'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'Civic',
//         anio: '2011'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Fiesta',
//         anio: '2020'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'City',
//         anio: '2014'
//     },
//     {
//         marca: 'Chevrolet',
//         modelo: 'Cruze',
//         anio: '2019'
//     },
//     {
//         marca: 'Fiat',
//         modelo: 'Cronos',
//         anio: '2018'
//     }
// ]
//
// // let nuevaListaAutosFiltrada = listaAutos.filter(auto => {
// //     return (auto.marca === 'Toyota' || auto.marca === 'Ford') && auto.anio > 2015;
// // });
//
// let nuevaListaAutosFiltrada = listaAutos.filter(auto => (auto.marca === 'Toyota' || auto.marca === 'Ford') && auto.anio > 2015);
//
// // let nuevaListaElectrodomesticos = listaElectrodomesticos.filter(palabra => {
// //     console.log(palabra);
// //     return palabra.length > 5;
// // });
//
// console.log(nuevaListaAutosFiltrada);
//

// // Map: crea un nuevo array con la modificacion de cada elemento
// let listaElectrodomesticos = ['heladera', 'microondas', 'horno', 'pava', 'extractor', 'batidora', 'licuadora'];
//
// // let nuevaListaElectrodomesticos = listaElectrodomesticos.map(electrodomestico => electrodomestico.toUpperCase());
// let nuevaListaElectrodomesticos = listaElectrodomesticos.map(function (electrodomestico) {
//     return electrodomestico.toUpperCase();
// });
//
//
// console.log(nuevaListaElectrodomesticos);

// // Reduce
// let listaNumeros = [1, 2, 15, 7, 5, 20];
//
// let funcionReduccion = function (acumulador, valorActual) {
//     return acumulador + valorActual;
// }
//
// let sumatoriaLista = listaNumeros.reduce(funcionReduccion);

// let sumatoriaLista = listaNumeros.reduce((acumulador, valorActual) => acumulador + valorActual);
//
// let sumatoriaLista = listaNumeros.reduce(function (acumulador, valorActual) {
//     return acumulador + valorActual;
// });

// console.log(sumatoriaLista);

// Find: devuelve el primer elemento que coincide con la funcion proporcionada
// let listaAutos = [
//     {
//         marca: 'Toyota',
//         modelo: 'Etios',
//         anio: '2018'
//     },
//     {
//         marca: 'Toyota',
//         modelo: 'Yaris',
//         anio: '2021'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Focus',
//         anio: '2012'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'Civic',
//         anio: '2011'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Fiesta',
//         anio: '2020'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'City',
//         anio: '2014'
//     },
//     {
//         marca: 'Chevrolet',
//         modelo: 'Cruze',
//         anio: '2019'
//     },
//     {
//         marca: 'Fiat',
//         modelo: 'Cronos',
//         anio: '2018'
//     }
// ]
//
// let autoBuscado = listaAutos.find(auto => {
//     console.log(auto);
//     return auto.marca === 'Toyota';
// });
//
// console.log('Se encontro el siguiente auto: ' + autoBuscado?.modelo);
//

// Some: devuelve booleano dependiendo hay un elemento que coincida con la condicion / Every: devuelve booleano dependiendo de si todos los elementos cumplen una condicion
// let listaAutos = [
//     {
//         marca: 'Toyota',
//         modelo: 'Etios',
//         anio: '2018'
//     },
//     {
//         marca: 'Toyota',
//         modelo: 'Yaris',
//         anio: '2021'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Focus',
//         anio: '2012'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'Civic',
//         anio: '2011'
//     },
//     {
//         marca: 'Ford',
//         modelo: 'Fiesta',
//         anio: '2020'
//     },
//     {
//         marca: 'Honda',
//         modelo: 'City',
//         anio: '2014'
//     },
//     {
//         marca: 'Chevrolet',
//         modelo: 'Cruze',
//         anio: '2019'
//     },
//     {
//         marca: 'Fiat',
//         modelo: 'Cronos',
//         anio: '2018'
//     }
// ]
//
// let hayAlgunAutoBuscado = listaAutos.some(auto => {
//     return auto.marca === 'Renault';
// });
//
// if (hayAlgunAutoBuscado) {
//     console.log('Hay un auto de marca Renault');
// } else {
//     console.log('No hay un auto de marca Renault');
// }

// let todosAutosMillenials = listaAutos.every(auto => auto.anio > 2000);
//
// if (todosAutosMillenials) {
//     console.log('Todos los autos son mill');
// } else {
//     console.log('No todos los autos son mill');
// }


// Funcion anonima
// let saludo = function (nombre) {
//     console.log('Hola ' + nombre);
// }
//
// saludo('Lautaro');

// (function (nombre){
//     console.log('Hola ' + nombre);
// })('Lautaro');

// (function (nombre) {
//     console.log('Hola ' + nombre);
// })('Lautaro');

// // Set Timeout
// function funcionTimeout() {
//     setTimeout(saludar, 1000);
// }
//
// let saludar = function () {
//     console.log('Hola lautaro despues de 5 segundos');
// }
//
// funcionTimeout();